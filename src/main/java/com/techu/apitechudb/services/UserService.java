package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;


    public List<UserModel>  sortByage() {
        System.out.println("order by age");
        return this.userRepository.findAll(Sort.by(Sort.Direction.ASC,"age"));
    }


    public List<UserModel> findAll() {
        System.out.println("find UserService");

        return this.userRepository.findAll();
    }

    public UserModel add(UserModel user){
        System.out.println("add en userService");

        return this.userRepository.save(user);
    }

    public Optional<UserModel> findById(String id){
        System.out.println("En findById de UserService");

        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel userModel){
        System.out.println("Update en UserSerice");

        return this.userRepository.save(userModel);
    }

    public boolean delete(String id){
        System.out.println("delete en UserService");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("Usuario encontrado borrado");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
